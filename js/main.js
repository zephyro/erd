

$(".btn_modal").fancybox({
    'padding'    : 0
});


$('.btn_send').click(function() {

    $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
    var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
    if(answer != false)
    {
        var $form = $(this).closest('form'),
            email           =     $('input[name="email"]', $form).val();


        $.ajax({
            type: "POST",
            url: "form-handler.php",
            data: {email:email}
        }).done(function(msg) {
            $('form').find('input[type=text], textarea').val('');
            console.log('удачно');
            $.fancybox.close();
            $('.btn_thanks').trigger('click');
        });
    }
});


$('.btn_accept').on('click touchstart', function(e){
    e.preventDefault();
    $(".privacy").hide('fast');
});

// Placeholders

$('.form_group input').focus(function(event) {
    $(this).closest('.form_group').addClass('focus');
});

$('.form_group input').focusout(function(){

    var inputVal = $(this).closest('.form_group').find('input').val();
    if (inputVal == '') {
        $(this).closest('.form_group').removeClass('focus');
    }
});


$(function() {

    // SVG IE11 support
    svg4everybody();


    if($.cookie('cookie_repeated')){

    }
    else {

        $(".privacy").addClass('first');
        $.cookie('cookie_repeated', 'yes', {
            expires: 7
        });
    }

});



