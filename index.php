<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">

        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>

        <header class="header">
	        <div class="container">
		        <div class="header_inner">
			        <div class="header_logo">
				        <img src="img/logo.png" class="img-fluid" alt="">
			        </div>

			        <h1>
				        Preliminary sign-up is open
				        <span>for a new, unique trading</span>
			        </h1>

			        <div class="header_text">
				        Simply predict the price change
				        and earn from <strong>€10</strong> to <strong>€1000</strong>.
				        You no longer need to guess
				        the direction of the chart.
			        </div>

			        <a href="#auth" class="btn btn_md btn_key btn_modal">
				        <i>
					        <svg class="ico-svg"  viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
						        <use xlink:href="img/sprite_icons.svg#icon_key" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
					        </svg>
				        </i>
				        <span>SIGN UP</span>
			        </a>

		        </div>
		        <div class="header_image">
			        <div class="header_image_wrap">
				        <img src="img/header_man.png" class="img-fluid" alt="">
			        </div>
			        <div class="header_quote">
				        <i>
					        <svg class="ico-svg"  viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
						        <use xlink:href="img/sprite_icons.svg#icon_quote" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
					        </svg>
				        </i>
				        <p>This is a real earnings tool.</p>
				        <p>For those who do not yet <br/>have <strong>€1,000,000</strong></p>
			        </div>
		        </div>
	        </div>
        </header>

        <section class="work">
	        <div class="container">
		        <div class="work_subtitle">We changed the principle of trading, simplified the interface, reduced risk, and multiplied your chances.</div>
		        <div class="h1">Here's how it works</div>

		        <div class="work_row">
			        <div class="work_graph">

				        <ul class="graph_legend">
					        <li>
						        <span>Investment</span>
						        <strong>€ 100</strong>
					        </li>
					        <li>
						        <span>Income per sec</span>
						        <strong>€ 10</strong>
					        </li>
					        <li>
						        <span>Total Income</span>
						        <strong>€ 120</strong>
					        </li>
				        </ul>

				        <div class="graph_mobile">
					        <img src="img/graph_mobile.png" class="img-fluid" alt="">
				        </div>

				        <div class="graph_desktop">
					        <img src="img/notebook.png" class="img-fluid" alt="">
				        </div>
			        </div>
			        <div class="work_content">
				        <ol class="work_step">
					        <li>
						        <strong>INVEST</strong>
						        <span>And lock in the price corridor</span>
					        </li>
					        <li>
						        <strong>EARN</strong>
						        <span>Every second while the price is inside the corridor</span>
					        </li>
					        <li>
						        <strong>GET PAID</strong>
						        <span>As soon as the price reaches the boundary of the corridor</span>
					        </li>
				        </ol>
			        </div>
		        </div>

	        </div>
        </section>

        <section class="bonus">
	        <div class="container">
				<div class="h1">Welcome bonus of <strong>100%</strong> on your deposit <span>only until the end of August</span></div>
		        <a href="#auth" class="btn btn_md btn_key btn_modal">
			        <i>
				        <svg class="ico-svg"  viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
					        <use xlink:href="img/sprite_icons.svg#icon_key" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
				        </svg>
			        </i>
			        <span>SIGN UP</span>
		        </a>
	        </div>
        </section>

        <footer class="footer">
	        <div class="container">
		        <div class="copy">© 2018 EDS TRADING</div>
	        </div>
        </footer>

        <div class="privacy">
	        <div class="container">
		        <h4>Cookies Policy</h4>
		        <p>We and our partners use technology such as cookies on our site to personalise content and ads, provide social media features, and analyse our traffic. Click below to consent to the use of this technology across the web. You can change your mind and change your consent choices at anytime by changing preferences in the browser settings.</p>
		        <a href="#" class="btn btn_dark btn_accept">Accept</a>
	        </div>
        </div>


        <div class="hide">
	        <div class="modal" id="auth">
		        <div class="modal_title">Registration</div>
		        <form class="form" name="form" method="POST" action="javascript:void(0);">
			        <div class="form_group valid email required">
				        <input class="form_control" type="text" name="email" placeholder="">
				        <div class="form_placeholder"><sup>*</sup>Email</div>
			        </div>
			        <div class="text-center">
				        <button type="submit" class="btn btn_dark btn_md btn_send">Registration</button>
			        </div>
		        </form>
	        </div>
        </div>


        <!-- Окно Спасибо -->
        <div class="hide">
	        <a href="#thanks" class="btn_thanks btn_modal"></a>
	        <div class="modal" id="thanks">
		        <div class="modal_title">Thank you</div>
		        <div class="modal_thanks">Your application is pending</div>
		        <br/>
		        <div class="text-center">
			        <button data-fancybox-close class="btn btn_dark btn_close">Close</button>
		        </div>
	        </div>
        </div>
        <!-- -->


        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/vendor/jquery.cookie.js"></script>
        <script src="js/vendor/validator.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
